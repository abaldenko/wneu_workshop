# shinyUI(fluidPage(
#   titlePanel("text-app"),
#   
#   sidebarLayout(
#     sidebarPanel(
#     uiOutput("Selector")
#     ),
#     
#     mainPanel(plotOutput("plot"), dataTableOutput("table"))
#   )
# ))

library(shinydashboard)
ui <- dashboardPage(
  dashboardHeader(title = "WNEU Workshop"),
  dashboardSidebar(
    sidebarMenu(id="menu1",
                menuItem("Question 1", tabName = "P", icon = icon("th")),
                menuItem("Question 2", tabName =  "Q", icon = icon("th")),
                menuItem("Question 3", tabName =  "R", icon = icon("th")),
                menuItem("Question 4", tabName =  "S", icon = icon("th")),
                menuItem("Question 5", tabName =  "T", icon = icon("th")),
                menuItem("Product Preference", tabName =  "U", icon = icon("th")),
                menuItem("Model", tabName =  "M", icon = icon("th")),
                menuItem("Overall", tabName =  "final", icon = icon("th"))
    )
  ),
  dashboardBody(
    #############
    # Question 1 : What do you think is the most common cause of Cell Phone Breaks ? 
    conditionalPanel(
      condition = "input.menu1 == 'P'",
      mainPanel(fluidRow(
          box(width = 6,title ="How many times have you broken your cell phone in the past year ?", 
                     tableOutput('P_questions'),
              helpText("Text your response to 845.208.8165")
          ),
          box(width=6, plotOutput("P_plot"))
          )  
        )
      ),
    #############
    # Question 2 : Which animal do you prefer?
    conditionalPanel(
      condition = "input.menu1 == 'Q'",
      mainPanel(fluidRow(
        box(width = 6,title ="Which animal do you prefer?", 
            tableOutput('Q_questions'),
            helpText("Text your response to 845.208.8165")
        ),
        box(width=6, plotOutput("Q_plot"))
      )  
      )
    ),
    #############
    # Question 3
    conditionalPanel(
      condition = "input.menu1 == 'R'",
      mainPanel(fluidRow(
        box(width = 6,title ="What do you think is the most common cause of Cell Phone Breaks ?",
            tableOutput('R_questions'),
            helpText("Text your response to 845.208.8165")
        ),
        box(width=6, plotOutput("R_plot"))
      )  
      )
    ),
    #############
    # Question 4
    conditionalPanel(
      condition = "input.menu1 == 'S'",
      mainPanel(fluidRow(
        box(width = 6,title ="How old are you?", 
            tableOutput('S_questions'),
            helpText("Text your response to 845.208.8165")
        ),
        box(width=6, plotOutput("S_plot"))
      )  
      )
    ),
    ###########
    # Question  5: What do you think is the most common cause of Cell Phone Breaks ? 
    conditionalPanel(
      condition = "input.menu1 == 'T'",
      mainPanel(fluidRow(
        box(width = 6,title ="What's your gender?", 
            tableOutput('T_questions'),
            helpText("Text your response to 845.208.8165")
        ),
        box(width=6, plotOutput("T_plot"))
      )  
      )
    ),
    ###########
    # Product Question: 
    conditionalPanel(
      condition = "input.menu1 == 'U'",
      mainPanel(fluidRow(
        box(width = 6,title ="Which product do you prefer?", 
            tableOutput('U_questions'),
            helpText("Text your response to 845.208.8165")
        ),
        box(width=6, plotOutput("U_plot"))
      )  
      )
    ),
    ###########
    # Model: Show plot of pre-trained decision tree
    conditionalPanel(
      condition = "input.menu1 == 'M'",
      mainPanel(fluidRow(
        box(width=12, plotOutput("model_plot"))
        )
      )
    ),  
    #############
    # Overall
    conditionalPanel(
      condition = "input.menu1 == 'final'",
      tableOutput('final_df')
    )
    
    
  ))