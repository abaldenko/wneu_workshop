source("curl.R")
library(ggplot2)
library(dplyr)
library(magrittr)
library(tidyr)
library(reshape2)
library(partykit)
library(randomForest)

texts <- get()
texts$time <- as.POSIXct(texts$time)
min <- min(texts$time)
max <- max(texts$time)


shinyServer(
  function(input, output, session) {
    
    # this will run every second
    # if checkFunc() is different than last time
    # then valueFunc() will be returned
    pollData <- reactivePoll(1000, session,
                             # This function returns the time that the logfile was last
                             # modified
                             checkFunc = function() {
                               nrow(get())
                             },
                             # This function returns the content of the logfile
                             valueFunc = function() {
                               df <- get() %>% 
                                 group_by(from, question) %>% 
                                 filter( time == max(time), question %in% c('P', 'Q', 'R', 'S', 'T', 'U')) %>% 
                                 select(-time) %>%
                               spread(question, response)
                               names(df) <- c('phone_number', 'num_breaks', 'funny', 'break_cause', 'age', 'gender', 'product')
                               df %<>% mutate(num_breaks = as.integer(num_breaks),
                                              funny = tolower(funny),
                                              break_cause = tolower(break_cause),
                                              gender = tolower(gender),
                                              age = as.integer(age)
                                              ) 
                               df
                             }
    )
    
    # render the selector
    output$Selector <- renderUI({
      selectInput("var", "Choose a sender to display:", c( as.list(pollData()$from), "ALL"), selected = "ALL") 
    })
    
    # render the plot
    output$plot <- renderPlot({
      texts <- pollData(); print(texts)
      texts$time <- as.POSIXct(texts$time)
      min <- min(texts$time)
      max <- max(texts$time)
    
      # all isn't a phone number, so treat it differently
      if(input$var == "ALL"){
        y <- texts
        print(texts)
      } else{
        y <- texts[texts$from == input$var,]
      }
      
      #plot
      ggplot(y, aes(x=time)) + geom_bar() + scale_x_datetime(limits = c(min, max)) + scale_y_continuous(limits=c(0,15))
      
    })
    
    # questions -------------------------
    output$P_questions <- renderTable({
      data.frame('Answer' = c('0 breaks',
                              '1 break', 
                              '2 breaks',
                              'ect.'),
                 'Text' = c('P 0','P 1','P 2', 'etc.')
                 ) 
      }, 
    include.rownames=FALSE)
    
    output$Q_questions <- renderTable({
      data.frame('Answer' = c('Cat',
                              'Dog'),
                 'Text' = c('Q cat','Q dog')
      ) 
    }, 
    include.rownames=FALSE)
    
    output$R_questions <- renderTable({
      data.frame('Answer' = c('Drop it while exercising',
                              'Sit on it', 
                              'Drop it in the toilet'
                              ),
                 'Text' = c('R a','R b','R c')
      ) 
    }, 
    include.rownames=FALSE)
    
    output$S_questions <- renderTable({
      data.frame('Answer' = c('19',
                              '23', 
                              'etc.'),
                 'Text' = c('S 19','S 23','etc.')
      ) 
    }, 
    include.rownames=FALSE)
    
    output$T_questions <- renderTable({
      data.frame('Answer' = c('male',
                              'female'),
                 'Text' = c('T m','T f')
      ) 
    }, 
    include.rownames=FALSE)
    
    output$U_questions <- renderTable({
      data.frame('Answer' = c('Product A',
                              'Product B'),
                 'Text' = c('T a','T b')
      ) 
    }, 
    include.rownames=FALSE)
    
    # Plots ----------------------------
    # P plot
    output$P_plot <- renderPlot({
      texts <- pollData() 
      texts %<>% mutate(num_breaks = as.numeric(num_breaks)) 
      ggplot(texts, aes(x = num_breaks)) + geom_histogram()
    })

    # Q plot
    output$Q_plot <- renderPlot({
      texts <- pollData() 
      texts %<>%  filter(funny %in% c('dog','cat')) 
      ggplot(texts, aes(x = funny)) + geom_bar()
    })
#     
    # R plot
    output$R_plot <- renderPlot({
      texts <- pollData() 
      texts %<>%  filter(break_cause %in% c('a','b','c')) 
      ggplot(texts, aes(x = break_cause)) + geom_bar()
    })
    
    # S plot
    output$S_plot <- renderPlot({
      texts <- pollData() 
      ggplot(texts, aes(x = age)) + geom_histogram()
    })
    
    # T plot
    output$T_plot <- renderPlot({
      texts <- pollData() 
      texts %<>%  filter(gender %in% c('m', 'f')) 
      ggplot(texts, aes(x = gender)) + geom_bar()
    })
    
    # U plot
    output$U_plot <- renderPlot({
      texts <- pollData() 
      texts %<>%  filter(product %in% c('a', 'b')) 
      ggplot(texts, aes(x = product)) + geom_bar()
    })
    
    output$model_plot <- renderPlot({
      plot(as.party(phone.model.tree))
    })
    
    # Final Plot
    output$final_df <- renderTable({
      df <- pollData() 
      
      #### clean rows
      clean <- df %>% filter(funny %in% c('dog','cat'),
                             break_cause %in% c('a','b','c'),
                             gender %in% c('m', 'f')) 
      
      # append rows with other factor levels
      n <- nrow(clean)
      cat(names(clean))
      cat(names(synth_data))
      clean <- rbind(clean, synth_data)
      
      # convert to factor
      clean$funny <- as.factor(clean$funny)
      clean$break_cause <- as.factor(clean$break_cause)
      clean$gender <- as.factor(clean$gender)
      
      # remove synthetic data
      clean <- clean[1:n,] 
      
      # predict
      clean$need_insurance <- predict(phone.model.forest, clean, type = "prob")[,2]
      
      #### deal with incomplete / broken records
      dirty <- df %>% anti_join(clean, by = "phone_number")
      dirty$need_insurance <- -1
      
      # combine clean and dirty
      final <- rbind(clean, dirty) %>% arrange(desc(need_insurance))
      final
    })
    
  }
  
)